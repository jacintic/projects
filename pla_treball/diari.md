# Diari

Aquest diari té per objectiu relatar tots els passos que ha seguit el nostre grup per al desenvolupament del projecte ABP.

## Projecte inicial
Entre tots vam acordar la idea del projecte basant-nos en els items necessàris descrits pels professors.
La aplicació web serà una plataforma educativa i contindrà llibreries de codi i tutorials.  
També es va decidir el nom del grup. Tot i que, aquest no té perque acabar sent el nom final del projecte.

## Arbre de continguts
Des de el mòdul de 'Disseny d'interfícies web se'ns va posar la tasca de crear un arbre de continguts.  
Mitjançant videoconferència vam anar generant-lo a partir de dels items expressats al projecte inicial.  
L'arbre es va dissenyar amb el software Dia.
Posteriorment, el Jacint va millorar la seva visibilitat usant el software Ilustrator.

## Servidor
Al modul de 'Desenvolupament web en entorn servidor' se'ns va oferir la opció d'usar una maquina virtual amb l'stack LAMP com a servidor web.  
Vam acordar treballar de la següent forma: Cada membre del grup faria proves amb la seva propia màquina virtual i desprès posariem en comú els descobriments i passos que va seguir cadascú.

## Replantejament de la idea inicial
Després de parlar amb el Dan Triano es va decidir que el projecte inicial era molt ambiciós. Per tant, es va reduïr el contingut d'aquest a pujades de tutorials i en cas de acabar amb temps, procediriem a realitzar els continguts restants.
Això va comportar un canvi de redacció del document "projecte inicial" i modificar l'arbre de continguts.
També s'ha decidit que per organitzar el projecte es crearien els següents fitxers: 
- **Projecte inicial:** D'escriu el projecte, les seves possibles ampliacions i el target a qui va destinada l'apllicació
- **Tasques:** Relació de tasques per fer, en procés i realitzades. 
- **Diari:** Document on es descriu com s'ha anat distribuïnt i desenvolupant el treball, problemes que ens em trobat i com es solventen.

## Login
El primer pas que ha demanat el Dan Triano es preparar un php que ens permeti fer un login.
Més endavant s'ha proposat un altre login, aquest cop utilitzant arrays per a multiples usuaris.
Tots els logins es troben a la secció de m07 d'aquest git.

## Wireframes
Al modul 09, disseny, se'ns ha demanat la realització de wireframes: Imatges que representen amb poc detall, com un esboç, les pàgines de l'aplicació web.
El procés per realitzar aquesta tasca s'ha realitzat primerament fent esboços a llapis i paper a classe entre tots els membres del grup. A casa cadascú feia els wireframes que, previameent, s'havien acordat de fer. A la següent classe es feia una possada en comú i si s'esqueia comentavem amb el Jordi Solà Laborda dubtes i millores.

## Sites
General / M07

## Templates
M09

## YouTrack
M05

## Mapa mental
M07 / General

## Primera implementació 
M07

## Modularitació
M06 / General -> YouTrack
