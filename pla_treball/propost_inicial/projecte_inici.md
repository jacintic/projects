# Grup
- Carlos Bernuy Obregon
- Jacint Iglesias Casanova
- Pau Vidal Roglan

# Projecte
Plataforma d'aprenentatge de programació amb llibreries i tutorials.

## Descripció 
És un projecte per fomentar l'aprenentatge de la programació.  
L'objectiu principal és aglutinar tot aquell contingut i informació que es troba dispers per la web en una sola plataforma educativa i centre de coneixement.  
El contingut de tutorials, ha de seguir una guia d'estil per oferir la informació de la plataforma d'una forma homogeneïtzada.   
Farem servir una base de dades tant per la gestió d'usuaris com per a contingut de la plataforma.  
El contingut del portal, incloent tots els apartats, haurà de ser respectuós i no es permetran insults o faltes de respecte.  

## Característiques
	- Tutorials
		- Tutorials en format text
	- Interacció entre usuaris
		- Possibilitat de pujar contingut
		- Valoració dels tutorials
		- Mostrar comentaris d'usuaris al contingut pujat pel professor (modera el mateix professor)

## Perspectiva d'ampliació
Si el temps ens ho permet voldrem dotar d'una sèrie d'eines útils als estudiants, com ara la llibreria amb programes i "snippets" de codi, vincular els repositoris dels usuaris de manera pública o privada, tutorials i altres continguts que descrivim més endavant.
Volem oferir als professors un espai on compartir el seu coneixement, donar classes i tutelar als seus alumnes.

	- Interacció entre usuaris
		- Donacions econòmiques d'alumnes a professors
		- Compartir codi entre professors i alumnes
		- Puntuacions de professors a codi d'alumnes
	- Llibreria 
		- Projectes
		- Programes
		- Snippets de codi
		- Enllaç a gits
		- Sistema de gestió per categories
		- Sistema de cerca
	- Tutorials
		- Videotutorials
		- Extrets o no d'altres pàgines webs (w3school, youtube...)
	- Classes magistrals en directe
	- Llistat de contactes
	- Missatgeria
		- Xats temàtics
		- Xats privats
		- Xat entre professors i alumnes
	- Contingut personalitzable segons necessitats de l'usuari (categories, tags...)

## Usuaris
- Alumnes !
	- Accés a  repositoris i llibreria 
	- Accés a tutorials !
	- Poden fer donacions als professors 
- Professors !
	- Pujar repositoris
	- Pujar tutorials !
	- Poden fer donacions
	- Puntuar alumnes !
	- Per a ser professor has d'exercir o tenir experiencia demostrable
- Administradors
	- El mateix que els anteriors 
	- Donen el vistiplau als professors
	- Control de contingut

(! -> la seva absencia indica que és fara a la prespectiva d'ampliació)

## Target  
Persones interessades en aprendre programació i en compartir el seu coneixement.   

### Edat  
Majors de divuit anys.    

### Sexe
Indiferent.    

### Nivell econòmic
Qualsevol persona amb connexió a internet i un ordinador o dispositiu mobil.  

### Per què l'usuari voldrà utilitzar la nostra plataforma?  
- Estudiants per aprendre i perfeccionar els seus coneixements de programació (o de seguretat).  
- Professors per altruisme o per benefici econòmic.
