# Qüestionari

**Quina finalitat té el lloc web i per a quina audiència s’ha fet?**
És un projecte per fomentar l'aprenentatge de la programació. 
L'objectiu principal és aglutinar tot aquell contingut i informació que es troba dispers per la web en una sola plataforma educativa i de coneixement.
El contingut de tutorials, ha de seguir una guia d'estil per oferir la informació de la plataforma d'una forma homogeneïtzada.
El contingut del portal, incloent tots els apartats, haurà de ser respectuós i no es permetran insults o faltes de respecte.

**Què s’ha fet per a protegir els drets d’altres autors?**
El nostre aplicatiu desitja compartir informació de forma interessada. 
Creiem que la millor forma d'assol·lir aquest objectiu es mitjançant la lliure del contingut que es puja a la nostra web.
Ens plantegem que el contingut pugui ser compartit mitjançant la llicencia CreativeCommons.

**Què s’ha fet per a evitar el plagi del vostre lloc web?**
El nostre web preten ser registrat sota la directiva de Creative Commons.
Marca que fidelize el producto.

**Raons per les que creieu que, basant-vos en els criteris d’avaluació, el vostre equip hauria de guanyar un dels premis del concurs.**
Creando propia empresa (para nosotros).
Accesibilitat.
Saber programar és una habilitat i eina cada dia més indispensable al món actual.
Creiem que l'aplicatiu Highcode pot ser una plataforma educativa d'aprenentatge i per compartir coneixements destinats a la programació.

**Si creus que sí, argumenta-ho.**
Creiem que a HighCode encara li queda molt camí de creixement i millora per assolir al màxim els nostres objectius.
