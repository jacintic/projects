# HighCode

[Enllaç presentació](https://prezi.com/view/IfT7sMaXgRzMhOsTfTCK/)

## Projecte

#### Que es?
High Code és una aplicació web per fomentar l'aprenentatge de la programació. Creada per Carlos Bernuy Obregon, Jacint Iglesias Casanova i Pau Vidal Roglan.
Preten aglutinar tot aquell contingut i informació que es troba dispers per la web en una sola plataforma educativa.

### Tecnologies
Per a desenvolupar el projecta hem utilitzat les tecnologies de: HTML, CSS, Javascript, Bootstrap i Laravel.
Ens centrarem en explicar aquesta última ja que és el pilar de los nostra app.

## Laravel
Laavel és un framework d'aplicacions web basat en PHP amb sintaxi expressiva i elegant. Proporciona un punt de partida per crear una aplicació web.  
Facilita que els desenvolupadors es dediqui'n a desenvolupar l'aplicació mentre Laravel es centra en els detalls. Es basa en el tipus ModelVistaControlador

#### Estructura

App -> Http -> Controllers / Requests
.   -> Exceptions / Models / Services

Directori bootstrap indicant que el tenim disponible.

Database -> Factories, migration, seeders

Public -> Assets, css, imatges, js...

Resources -> On trobem les vistes

Routes -> Dintre hi ha l'archiu web.php que serà com la guia del routting

Altres interessants
composer.json per fer el install, el .env que no es veu, artisan que es la interface de lines de comandes a Laraver.

## Rutes

Les routes defineixen el comportament de la nostra app.
El post i get diferencien com rep el servidor aquestes dades.


## Vistes

Separem entre layouts i views.

Parlar de layout, section, yield...

