@extends('layouts.noLoginWithSidebar')

@section('title')
Welcome to HighCode!
@endsection


@section('styles')
<link rel="stylesheet" type="text/css" href="css/style.css">
@endsection

@section('content')
<div class="cardContainer">
        <div class="cards">
            <a href="/tutorials/javascript">
                <div class="card c1">
                    <h2>JavaScript</h2>
                </div>
            </a>
            <a href="/tutorials/java">
                <div class="card c2">
                    <h2>Java</h2>
                </div>
            </a>
            <a href="/tutorials/laravel">
                <div class="card c3">
                    <h2>Laravel</h2>
                </div>
            </a>
            <a href="/tutorials/vue">
                <div class="card c4">
                    <h2>Vue</h2>
                </div>
            </a>
            <a href="/tutorials/bash-script">
                <div class="card c5">
                    <h2>Bash Script</h2>
                </div>
            </a>
            <a href="/tutorials/bootstrap">
                <div class="card c6">
                    <h2>Bootstrap</h2>
                </div>
            </a>
        </div>
    </div>
@endsection
