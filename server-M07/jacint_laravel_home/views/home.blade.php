@extends('layouts.app')

@section('title')
Welcome to HighCode!
@endsection


@section('styles')
<link rel="stylesheet" type="text/css" href="css/style.css">
@endsection

@section('content')
<div class="cardContainer">
        <div class="cards">
            <a href="/javascript">
                <div class="card c1">
                    <h2>JavaScript</h2>
                </div>
            </a>
            <a href="/java">
                <div class="card c2">
                    <h2>Java</h2>
                </div>
            </a>
            <a href="/laravel">
                <div class="card c3">
                    <h2>Laravel</h2>
                </div>
            </a>
            <a href="/vue">
                <div class="card c4">
                    <h2>Vue</h2>
                </div>
            </a>
            <a href="/bash-script">
                <div class="card c5">
                    <h2>Bash Script</h2>
                </div>
            </a>
            <a href="/bootstrap">
                <div class="card c6">
                    <h2>Bootstrap</h2>
                </div>
            </a>
        </div>
    </div>
@endsection