<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('', function () {
	return view('home');
	});
Route::get('/', function () {
    return view('home');
});
Route::get('helloworld', function () {
	return '<h1>Hello World!</h1>';
	});
Route::get('test', function () {
	return view('test');
	});
Route::get('form', function () {
	return view('form');
	});
Route::post('/result', function() {
	$nom = $_POST['nom'];
	return view('result', compact('nom'));
	// no he conseguit que funcioni
	//return view('result'->with('nom',$nom));
});