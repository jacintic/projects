<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Database\QueryException;
use App\Services\UploadPhotographyService;
use App\Exceptions\UserPhotoException;

class UserController extends Controller
{
    public $userModel;

    // Constructor
    public function __construct()
    {
        $this->userModel = new User();
    }

    //Insert user
    public function addUser(UserRequest $r, UploadPhotographyService $service)
    {
        try {
            //dd($userData->all()); //<--- for checking data Request 
            //Usamos un servicio que hemos creado para guardar fotos
            $service->uploadFile($r->file('imagen'));

            $user = new User;
            $user->username = $r["username"];
            $user->email = $r["email"];
            $user->password = $r["password"];
            $user->role = "student";
            $user->imagen = $r["imagen"]->getClientOriginalName();
            $success = $user->save();
            //Si no ha seleccionado ninguna foto
        } catch (UserPhotoException $exception) {
            $message = $exception->customMessage();
            return back()->withError($message)->withInput();
            //Si el usuario ya existe en la BBDD
        } catch (QueryException $exception) {
            return back()->withError("Usuario o Email ya registrados")->withInput();
        }
        return redirect()->action(
            [UserController::class, 'new'],
            ['success' => $success]
        );
    }

    //View of form to sing Up
    public function new()
    {
        return view('forms/signUp');
    }

    //Login user
    public function loginRequest(LoginRequest $loginData)
    {
        //dd($loginData);
        $filters = $loginData->except(['_token']);
        $userAndPass = collect($filters)->filter()->all();
        $userdata = User::login($userAndPass);
        $userprocessed = [];
        $message = '';
        if (sizeof($userdata) != 0) {
            $userprocessed = [
                'user' => $userdata[0]->username,
                'pass' => $userdata[0]->password,
            ];
            if ($userprocessed["user"] == $userAndPass['username'] && $userprocessed["pass"] == $userAndPass['password']) {
                $message = 'Welcome ' . $userprocessed["user"] . ', logging successful.';
            }
        } else {
            $message = 'ERROR: wrong login or password.';
        }

        return view('confirmation/loginProcess', compact('message'));
    }
}
