<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Validate the data 
            'username' => 'required|max:20',
            'email' => 'required',
            'password' => 'required|min:3',
            //'imagen' => 'required'
        ];
    }

    public function messages() {
        return [
            'username.required'=>'Siusplau, insereix un :attribute.',
            'username.max'=>"El :attribute no pot superar els 20 caràcters.",
            'email.required'=>'Siusplau, insereix un :attribute.',
            'password.required'=>'Sisuplau, insereix una :attribute.',
            'password.min'=>'La :attribute ha de superar els 3 craràcters.',
            //'imagen.required'=>'Es necesario seleccionar una foto'
        ];
    }

    public function attributes() {
        return [
            'username'=>"nom d'usuari",
            'email'=>'correu electrònic',
            'password'=>'contrassenya'
        ];
    }
}
