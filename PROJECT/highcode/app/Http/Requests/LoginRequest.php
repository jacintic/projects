<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Validate the data 
            'username' => 'required|max:20',
            'password' => 'required|min:3',
        ];
    }
    public function messages() {
        return [
            'username.required'=>'Siusplau, insereix un :attribute.',
            'username.max'=>"El :attribute no pot superar els 20 caràcters.",
            'password.required'=>'Sisuplau, insereix una :attribute.',
            'password.min'=>'La :attribute ha de superar els 3 craràcters.'
        ];
    }

    public function attributes() {
        return [
            'username'=>"nom d'usuari",
            'password'=>'contrassenya'
        ];
    }
}
