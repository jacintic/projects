<?php

//namespace Database\Seeders; //<=== esto da error si lo descomentas
use App\Models\Tutorial;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(TutorialSeeder::class);
         Tutorial::factory(20)->create();
    }
}        
    class TutorialSeeder extends Seeder
    {
        public function run(){
     
            DB::table('users')-> insert([
                [
                    'username' => 'Pakirrin Johnson',
                    'email' => 'pj@gmail.com',
                    'created_at' => date("Y-m-d H:i:s"),
                    'password' => "pepe",
                    'role' => 'student'                    
                ],
                [
                    'username' => 'Pakirringo Star',
                    'email' => 'ps@gmail.com',
                    'created_at' => date("Y-m-d H:i:s"),
                    'password' => "pepe",
                    'role' => 'teacher'                    
                ],
                [
                    'username' => 'Manolo Perez',
                    'email' => 'mp@gmail.com',
                    'created_at' => date("Y-m-d H:i:s"),
                    'password' => "pepe",
                    'role' => 'teacher'                    
                ]
            ]);
            
 

            // DB::table('tutorials')->insert([
            //         [
            //             'tech' => 'PHP',
            //             'author' => 1,
            //             'title' => 'Hashing passwords',
            //             'diff' => 'high',
            //             'duration' => 'short',
            //             'desc' => 'This is a short description',
            //             'body' => 'This is the body of the tutorial',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ],
            //         [
            //             'tech' => 'Laravel',
            //             'author' => 2,
            //             'title' => 'How to use the blade engine',
            //             'diff' => 'medium',
            //             'duration' => 'medium',
            //             'desc' => 'How to seed a database.',
            //             'body' => 'This is danger test territory <h1>COOL</h1>',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ],
            //         [
            //             'tech' => 'Javascript',
            //             'author' => 1,
            //             'title' => 'Loops',
            //             'diff' => 'easy',
            //             'duration' => 'long',
            //             'desc' => 'How to create an infinite loop in javascript using for loops.',
            //             'body' => 'Just us for within for within for, it works every time. <br>More test stuff <h3>hello!</h3>.',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ],
            //         [
            //             'tech' => 'Vue',
            //             'author' => 2,
            //             'title' => 'Events in Vue',
            //             'diff' => 'medium',
            //             'duration' => 'medium',
            //             'desc' => 'Creating a todo app.',
            //             'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ],
            //         [
            //             'tech' => 'Vue',
            //             'author' => 3,
            //             'title' => 'Events in Vue',
            //             'diff' => 'easy',
            //             'duration' => 'short',
            //             'desc' => 'Creating a todo app.',
            //             'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ],
            //         [
            //             'tech' => 'Vue',
            //             'author' => 1,
            //             'title' => 'Events in Vue',
            //             'diff' => 'medium',
            //             'duration' => 'short',
            //             'desc' => 'Creating a todo app.',
            //             'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ]
            //         ,
            //         [
            //             'tech' => 'Vue',
            //             'author' => 3,
            //             'title' => 'Events in Vue',
            //             'diff' => 'high',
            //             'duration' => 'short',
            //             'desc' => 'Creating a todo app.',
            //             'body' => 'Lorem ipsum dolor amet larari larara tal qual pascual. Torrijas con leche azucar pan y aceite.',
            //             'created_at' => date("Y-m-d H:i:s")
            //         ]
            //     ]);  
        }
    }
