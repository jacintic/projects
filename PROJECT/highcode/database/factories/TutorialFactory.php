<?php

namespace Database\Factories;

use App\Models\Tutorial;
use Illuminate\Database\Eloquent\Factories\Factory;

class TutorialFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tutorial::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
        'tech' => $this->faker->randomElement(['JavaScript','Java','Laravel','Vue','BashScript','Bootstrap']),
        'author' => $this->faker->randomElement(['1','2','3']),
        'title' => $this->faker->randomElement(['Title_1','Title_2','Titile_3']),
        'diff' => $this->faker->randomElement(['Easy','Medium','High']),
        'duration' => $this->faker->randomElement(['Short','Medium','Long']),
        'desc' => $this->faker->sentence(),
        'body' => $this->faker->paragraph(),
        'precio' => $this->faker->randomFloat($nbMaxDecimals = 3, $min = 0, $max = 100) // 48.8932

        //'created_at' => $this->fake->            
        ];
    }
}
