<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CreateTutorialsTable extends Factory 
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function definition()
    {
        return [ 
        'username' => $this->faker->randomElement(['Carlos','Jacint','Pau']),
        'email' => $this->faker->randomElement(['carlos@gmail.com','jacint@hotmail.com','pau@yahoo.com']),
        'password' => $this->faker->randomElement(['asdf','qwer','ty','sfghf','rtywrty','wgfhfgh']),
        'role' => $this->faker->randomElement(['student','teacher'])
        ];
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tutorials');
    }
}
