@extends('layouts.filter')

@section('content')

    <div class="container-fluid bg-light">
        <br>
        {{-- Formulario --}}
        <div class="row mx-3">
            <form method="get" action="/search" class="form-inline my-2 my-lg-0">
                <select name="tipo" class="form-control rounded" id="exampleFormControlSelect1">
                    <option value="title">Titulo</option>
                    <option value="author">Autor</option>
                    <option value="tech">Tech</option>
                </select>
                <input class="form-control mx-3 rounded" type="search" placeholder="Search" aria-label="Search"
                    name="buscar">
                <button class="btn btn-outline-success btn-sm px-4 rounded" type="submit"><strong>Search</strong></button>
            </form>
        </div>
        {{-- tutoriales y favoritos --}}
        <div class="row">
            {{-- tutoriales --}}
            <div class="col-lg-9 col-md-9 col-sm-12">
                <div id="mycontent">
                    @forelse ($tutoriales as $tutorial)
                        <div class="row border border-primary rounded bg-white m-3 p-2">
                            {{-- Foto y usuario --}}
                            <div class="col-lg-2 col-md-3 col-sm-3 col-5">
                                <div class="row">
                                    <a href="tutorials/show/{{ $tutorial->id }}">
                                        <img src="{{ asset('/img/UserImg.png') }}" class="img-fluid" alt="Imagen">
                                    </a>
                                </div>
                                <div class="row justify-content-center">
                                    <span>{{ $tutorial->username }}</span>
                                </div>
                            </div>
                            {{-- Descripcion --}}
                            <div class="col-lg-9 col-md-8 col-sm-8">
                                <div class="row justify-content-center">
                                    <a href="tutorials/show/{{ $tutorial->id }}">
                                        <h2>{{ $tutorial->title }}</h2>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        <div class="text-center rounded py-2">
                                            <span class="my-2">{{ $tutorial->diff }}</span><br>
                                            <span class="my-2">{{ $tutorial->duration }}</span><br>
                                            <span class="my-2"><i>{{ date('d-m-yy', strtotime($tutorial->created_at)) }}
                                                </i></span><br><br>
                                            <span class="border border-dark rounded p-1">{{ $tutorial->tech }}</span><br>

                                            {{-- <span>{{$tutorial->created_at}}</span><br> --}}
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <a href="tutorials/show/{{ $tutorial->id }}">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo ipsam iure hic
                                                aspernatur!
                                                Amet
                                                veritatis cumque vel eveniet, nobis ut architecto voluptatibus fugit laborum cum
                                                suscipit
                                                hic
                                                exercitationem quam non?</p>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            {{-- botton seguir --}}
                            <div class="col-lg-1 col-md-1 col-sm-1 col align-self-end">
                                <form method="post" action='followTutorial'>
                                    {{ csrf_field() }}
                                    <input type="hidden" name="title" value="{{ $tutorial->title }}">
                                    <input type="hidden" name="tech" value="{{ $tutorial->tech }}">
                                    {{-- <input type="submit" value="Add product" class="btn btn-primary"> --}}
                                    <div class="row justify-content-end">
                                        <input type="submit" value="Follow" class="btn btn-primary btn-sm">
                                    </div>
                                </form>
                            </div>
                        </div>
                    @empty
                        <h2>Oops, no tutorials here yet!</h2>
                    @endforelse
                </div>

            </div>
            {{-- favoritos --}}
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="row justify-content-center">
                    <h1 class="display-5 mb-4">Followeds
                    </h1>
                </div>
                @if (!session()->get('seguidos', []))
                    <div class="row justify-content-center">
                        <span>No hay favoritos</span>
                    </div>
                @endif
                <div class="row justify-content-center">
                    {{-- @forelse (app('request')->session()->get('seguidos',[]) as $producto) --}}
                    @forelse (session()->get('seguidos',[]) as $producto)
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <strong>
                                    <h5 class="card-title">{{ $producto[0] }}</h5>
                                </strong>
                                <p class="card-text">{{ $producto[1] }}</p>
                                <a href="#" class="text-white bg-dark p-1 rounded">Ver tutorial</a>
                                <button class="rounded btn-warning ml-4">Eliminar</button>
                            </div>
                        </div>
                    @empty
                    @endforelse
                </div>
                @if (session()->get('seguidos', []))
                    <div class="row justify-content-center">
                        <a href='removeFollows'><button type="button" class="btn btn-danger btn-sm">Eliminar todo</button></a>
                    </div>
                @endif
            </div>
        </div>
    </div>

@endsection
