@extends('layouts.app')                                                         
@section('title', 'Sign Up')                                                    
@section('styles')                                                              
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styleforms.css') }}">
@endsection                                                                     
@section('content')  
    <div class="container">                                                       
        <div class="row">                                                           
            <div class="col-lg-10 col-xl-9 mx-auto">                                  
                <div class="card card-signin flex-row my-5">                            
                    <div class="card-img-left d-none d-md-flex">                          
                        <!-- Background image for card set in CSS! -->                     
                    </div>                                                                
                    <div class="card-body">                                               
                        <h5 class="card-title text-center">Log In</h5>                      
                        <form class="form-signin" method="post" action="/loginUser">
                        @csrf                                          
                            <div class="form-label-group">                                    
                                <input type="text" id="inputUserame" class="form-control" placeholder="Username" required autofocus name="username">
                                <label for="inputUserame">Username</label> 
                                @error('username')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror                     
                            </div>                                                            

                            <div class="form-label-group">                                    
                                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required name="password">
                                <label for="inputPassword">Password</label> 
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror                    
                            </div>                                                            
                            <hr class="my-4">                                                 
                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Log In</button>
                            <a class="d-block text-center mt-2 small" href="/users/new">Sign Up</a>       
                        </form>                                                             
                    </div>                                                                
                </div>                                                                  
            </div>                                                                    
        </div>                                                                      
    </div>       
@endsection
