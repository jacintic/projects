@extends('layouts.app')

<!-- title-->
@section('title', 'Create a Tutorial!')

<!-- cards home css-->
@section('styles')
<!--<link rel="stylesheet" type="text/css" href="css/createTutorial.css">-->
@endsection

<!-- cards home html-->
    @section('content')    
    <div class="container border border-secondary">
        <div class="row justify-content-center p-3 m-3">            
            <form method="post" action="/insertedTutorial" class="p-3">            
            @csrf
            <div class="form-row">
                <div class="col-10">
                    <label for="titol">Titol</label>
                    <input type="text" name="title" class="form-control">
                @error('title')
                    <br>
                    <div class="alert alert-danger">{{ $message }}</div>
                    <br>                  
                @enderror
                </div>
                <div class="col-2">
                <label for="titol">ID</label>
                    <select name="author" class="custom-select">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    </select>
                </div>
            </div>
            <div class="form-row mt-3">                
                <div class="col-auto">
                    <label for="titol">Tecnologia</label>
                    <select name="tech" class="custom-select">
                    <option value="JavaScript">JavaScript</option>
                    <option value="Laravel">Laravel</option>
                    <option value="Java">Java</option>
                    <option value="Boostrap">Boostrap</option>
                    <option value="Vue">Vue</option>
                    <option value="BashScript">BashScript</option>
                    </select>
                </div>
                <div class="col-auto">
                    <label for="titol">Dificultad</label>                 
                    <select name="diff" class="custom-select">
                    <option value="Easy">Easy</option>
                    <option value="Medium">Medium</option>
                    <option value="High">High</option>           
                    </select>
                </div>               
                <div class="col-auto">
                    <label for="titol">Duracion</label>  
                    <select name="duration" class="custom-select">
                    <option value="Short">Short</option>
                    <option value="Medium">Medium</option>
                    <option value="Long">Long</option>           
                    </select><br>
                </div>               
            </div>
            <div class="form-row mt-3">       
                                       
                <label for="titol">Descripció:</label><br>  
                <textarea name="desc" id="" cols="60" rows="3" class="form-control"></textarea><br>
                @error('desc')
                <div class="alert alert-danger">{{ $message }}</div>
                <br> 
                <br>
                @enderror
                            
            </div>
            <div class="form-row mt-3 mb-3">                
                <label for="titol">Cos del tutorial:</label><br> 
                <textarea name="body" id="" cols="50" rows="20" class="form-control"></textarea><br>
                @error('body')
                <div class="alert alert-danger">{{ $message }}</div>
                <br>  
                @enderror
                <!--
                @if ($errors->any())
                    <div class="alert alert-danger col-3">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                    </div>
                @endif
                -->
            </div>
            <input type="submit" value="Enviar" class="btn btn-primary">
            </form>
        </div>
    </div>
    @endsection
