<!-- Layout with only navbar, styles and js-->
@extends('layouts.app')

<!-- title-->
@section('title', 'Highcode - User registered successfully')

<!-- user registration successful html -->
    @section('content')
    <h1>User registered successfully!</h1>
    @endsection