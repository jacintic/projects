<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Axios;
use App\Http\Controllers\TutorialController;
use App\Http\Controllers\UserController;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route to the main page (root)
Route::get('/', function () {
    return view('home');
});


//---- USERS LOGIN/SIGN UP ----//
// Login
Route::get('/login', function() {
    return view('forms/logIn');
});
// Welcome user
Route::post('/loginUser', [UserController::class,'loginRequest']);

// Sing up
Route::get('/users/new', [UserController::class, 'new']);
Route::post('/users/new', [UserController::class, 'addUser']) ->name('users.addUser');

//----- USER SETTINGS -------//
Route::get('/settings', function() {
    return view ('forms/userSettings');
});

//----- BECOME TEACHER -------//
Route::get('/becomeTeacher', function() {
    return view('forms/becomeTeacher ');
});

//---- TUTORIAL----//
// Create a tutorial
Route::get('/createTutorial', function() {
    return view('forms/createTutorial');
});

// Confirm added tutorial
Route::post('/insertedTutorial', [TutorialController::class,'addTutorials']);

// Follow / unFollow tutorials
Route::post('/followTutorial', [TutorialController::class, 'followTutorial']);
Route::get('/removeFollows', [TutorialController::class, 'removeFollows']);

//----FILTROS----//
// Show a tutorial
Route::get('/tutorials/show/{id?}',[TutorialController::class, 'seeTutorial']);
// Sidebar filter
Route::get('filter', [TutorialController::class,'filterForm']);
// SearchBar filter
Route::get('search', [TutorialController::class,'filterSearch']);
// Show tutorials by tech
Route::get('/{lang?}', [TutorialController::class,'showTutorials']);

