require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");
// mongoose
var mongoose = require('mongoose');
// bodyParser
var bodyParser = require ('body-parser');
app.use(bodyParser());
//Layouts
const expressLayouts = require('express-ejs-layouts');
// cookies
var cookieParser = require("cookie-parser");
// socket
var server = require('http').createServer(app);
var io = require("socket.io")(server);


//Motor plantillas EJS y Layouts
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(expressLayouts);

//css & js
app.use(express.static(__dirname + '/public'));

//Puerto
var server = require("http").createServer(app).listen(port, ()=> {
    console.log("Puerto conectado: " + port)
});

//Rutas
// Enlacamos archivos
// var indexRouter = require("./app/routes/base");
// var rutaUsers = require("./app/routes/users")
// app.use('/', indexRouter);
var api = require("./app/routes/api");
var rutaIncidencias = require("./app/routes/incidencias");
var rutaChat = require("./app/routes/chat");

// Home
app.route('/').get((req, res, next) => {
    res.render("index", {title: "Home"})
})
// Incidencias
app.use('/incidencias', rutaIncidencias);
//Chat
app.use('/chat', rutaChat);

// //API
app.use('/api',(req, res, next) =>{
    req.isApi=true;
    next();
  });
app.use('/api',api);

//Error 404
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/views/404.html');
});

// cookies
app.use(cookieParser());


//Connect to mongodb://devroot:devroot@mongo:27017/chat?authSource=admin
mongoose.connect(
    `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
    { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
    (err, res) => {
    if (err) console.log(`ERROR: connecting to Database. ${err}`);
    else console.log(`Database Online: ${process.env.MONGO_DB}`);
    }
    );
    console.log(`mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`);
    
io.on("connection", (socket) => {
    console.log("conectado");
    socket.user="Pedro";
    socket.on("newMsg", (data) => {
        console.log(data);
        socket.broadcast.emit("newMsg", data);
    })
})
