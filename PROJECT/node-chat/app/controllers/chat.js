var mongoose = require("mongoose"),
    Chat = require("../../models/Chat");

// load from DB function
exports.load = async () => {
    var res = await Chat.find({});
    return res;
}
// save to DB function
exports.save = (req) => {
    var newChat = new Chat(req);
    newChat.save((err,res)=>{
        if(err) console.log(err);
        console.log("Insertando en la BD");
        return res;
    })
}


/*
exports.getAll = (req, res, next) => {
    var chat = [
        {
            id: 1,
            texto: ["prueba", "prueba"],
            users: [2,1]
        },
    ];
    return res.json(chat);
}

exports.getChat = (req, res, next) => {
    var chat = [
        {
            id: 1,
            texto: ["pregunta", "respuesta"],
            users: [2,1]
        },
    ];
    return res.json(chat);
};

exports.search = (req, res, next) => {
    var chat = [
        {
            id: 1,
            texto: ["pregunta", "respuesta"],
            users: [2,1]
        },
    ];
};

exports.new = (req, res, next) => {
    return res.json("false")
};

exports.delete = (req, res, next) => {
    return res.json("true");
}

exports.update = (req, res, next) => {
    return res.json("false");
};
*/
