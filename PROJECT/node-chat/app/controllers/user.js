User = require("../models/User");

exports.loadAll = async (req, res, next) => {
  try {
    const users = await User.find();
    console.log(users);
    if (req.isApi) {
      res.json(users);
    } else {
      return users;
    }
  } catch (error) {
    console.log(error);
  }
}

exports.loadOne = async (req, res, next) => {
  try {
    const user = await User.findById(req.params.id);
    if (req.isApi) {
      res.json(user);
    } else {
      return user;
    }
  } catch (error) {
    console.log(error);
  }
};


exports.save = async (req, res, next) => {
  const asignatura = new User({
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    dni: req.body.dni
  })
  try {
    const guardado = await asignatura.save();
    if (req.isApi) {
      res.status(201).json(guardado);
    } else {
      return guardado;
    }
  } catch (error) {
    console.log(error);
  }
}

exports.delete = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const resultado = await user.delete();
    console.log(resultado);
    if (req.isApi) {
      res.json({ mesagge: "Eliminado" });
    } else {
      return "Eliminado";
    }
  } catch (error) {
    console.log(error);
  }
}


exports.update = async (req, res) => {
  const user = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    dni: req.body.dni
  }
  try {
    const actu = await User.updateOne({ _id: req.params.id }, user)
    if (req.isApi) {
      res.status(200).json({ mesagge: "Actualizado" });
    } else {
      return "Actualizado";
    }
  } catch (error) {
    console.log(error);
  }
}