//var mongoose = require("mongoose"),
const Incidencia = require("../models/Incidencia");

//// cargar todas las asignaturas
exports.loadAll = async (req, res, next) => {
    try {
        const incidencia = await Incidencia.find();        
        if (req.isApi) {
            res.json(incidencia);
        } else {
            return incidencia;
        }        
        return incidencia;
    } catch (error) {
        console.log(error);
      //Puede ser otro tipo de respuesta
    } 
}

//cargar una asignatura
exports.loadOne = async (req, res, next) => {
    try {
        const incidencia = await Incidencia.findById(req.params.id);
        console.log(incidencia);
        if(req.isApi) {
            res.json(incidencia);
        } else {
            return incidencia;
        }
    } catch (error) {
        console.log(error); 
    } 
}

// guardar asignatgura
exports.save = async (req, res, next) => {
    const incidencia = new Incidencia({
        nombre: req.body.nombre,
        dni: req.body.dni,
        tipo: req.body.tipo,
        descripcion: req.body.descripcion,
        urgencia: req.body.urgencia
    })
    try {
        const guardado = await incidencia.save();
        if (req.isApi) {
            res.status(201).json(guardado);
        } else {
            return guardado;
        }
    } catch (error) {
        console.log(error);
    }
}

//Eliminar asignatura
exports.delete = async (req, res) => {
    try {
        const incidencia = await Incidencia.findById(req.params.id);
        const resultado = incidencia.delete();
        if (req.isApi) {
            res.json({ mesagge: "Eliminado"});
        } else {
            return "Eliminado";
        }        
    } catch (error) {
        console.log(error); 
    } 
}

//Actualizar asignatura
exports.update = async (req,res) => {
    const incidencia = {
        nombre: req.body.nombre,
        dni: req.body.dni,
        tipo: req.body.tipo,
        descripcion: req.body.descripcion,
        urgencia: req.body.urgencia
    }
    try {
        const actu = await Incidencia.updateOne({_id: req.params.id}, incidencia)
        if(req.isApi) {
            res.status(200).json({ mesagge: "Actualizado"});
        } else {
            return "Actualizado";
        }
    } catch (error) {
        console.log("error updating");
        console.log(error);
    }
}