var mongoose = require('mongoose');
Schema = mongoose.Schema;

var userSchema = new mongoose.Schema({
    nombre: { type: String },
    apellido: { type: String},
    dni: { type: String },    
    created_at: { type: Date, default: Date.new }
});

module.exports = mongoose.model("User", userSchema);
