var mongoose = require('mongoose');
Schema = mongoose.Schema;

var incidenciaSchema = new mongoose.Schema({
    nombre: { type: String },
    dni: { type: String },
    tipo: { type: String },
    urgencia: { type: String },
    descripcion: { type: String },
    created_at: { type: Date, default: Date.new }
});

module.exports = mongoose.model("Incidencia", incidenciaSchema);
