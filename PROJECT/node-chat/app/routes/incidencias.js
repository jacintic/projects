var express = require("express");
var router = express.Router();
//Controladores
var path = require("path");
var ctrlDir = "/app/app/controllers";
var incidenciasCtrl = require(path.join(ctrlDir, "incidencias"));
// cookies
var app = express();
var cookieParser = require("cookie-parser");
app.use(cookieParser());

router.get("/", function(req, res, next) {
    res.render("incidencias", {guardado: false})
});

/** ListaIncidencias */
router.get("/listaIncidencias", async function(req, res, next) {
    incidencias = await incidenciasCtrl.loadAll(req);
    res.render("listaIncidencias", {incidencias: incidencias, borrado : false});
});

/** Encidencia enviada */
router.post("/incidenciaEnviada", async function(req, res, next) {
    result = await incidenciasCtrl.save(req);
    if (result) {
        var guardado = true;
    }
    res.render("incidencias", {guardado: guardado});
})

/** ModificarIncidencias */
router.get("/modifIncidencia/:id", async function(req, res, next) {
    incidencias = await incidenciasCtrl.loadOne(req);
    res.render("modIncidencias", {'incidencias': incidencias, 'guardado': false});
});

/** Incidencia modificada */
router.post("/incidenciaModificada", async function(req, res, next) {
    req.params.id = req.body.id;
    result = await incidenciasCtrl.update(req);
    var guardado = false;
    if (result) {
        var guardado = true;
    }
    var incidencias = await incidenciasCtrl.loadOne(req);
    res.render("modIncidencias", {'incidencias': incidencias, 'guardado': guardado});
})

// delete incidencias
router.get("/deleteIncidencia/:id", async function(req, res, next) {
    incidencias = await incidenciasCtrl.delete(req);
    res.redirect("../listaIncidencias");
});
module.exports = router;