var path = require("path");
var ctrlDir = "/app/app/controllers";
var express = require("express");
var router = express.Router();
//Controladores
var userCtrl = require(path.join(ctrlDir, "user"));
var incidenciasCtrl = require(path.join(ctrlDir, "incidencias"));
var chatCtrl = require(path.join(ctrlDir, "chat"));


// Users
//Get all
router.get("/users", userCtrl.loadAll);
//Get user by id
router.get("/users/:id", userCtrl.loadOne);
//Insert user
router.post("/users", userCtrl.save);
//Delete user
router.delete("/users/:id", userCtrl.delete);
//Put user
router.put("/users/:id", userCtrl.update);

// Incidencias
//Get all
router.get("/incidencias", incidenciasCtrl.loadAll);
//Get user by id
router.get("/incidencias/:id", incidenciasCtrl.loadOne);
//Insert user
router.post("/incidencias", incidenciasCtrl.save);
//Delete user
router.delete("/incidencias/:id", incidenciasCtrl.delete);
//Put user
router.put("/incidencias/:id", incidenciasCtrl.update);

module.exports = router;
