
var express = require("express");
var router = express.Router();

// controllers and paths
var path = require("path");
var ctrlDir = "/app/app/controllers";
var userCtrl = require(path.join(ctrlDir, "user"));
var chatCtrl = require(path.join(ctrlDir, "chat"));


/** Chat */
router.get("/", async function(req, res, next) {
    console.log("Cargando chat");
    result = await chatCtrl.load();
    console.log(result);
    res.render("chat", result);
});


router.route('new').get((req,res,next) => {
    var newChat = {
        user1: '/user/1',
        user2: '/user/2',
        msg: [{
            user: '1',
            texto: "hola que tal"
        },
        {
            user: '2',
            texto: "be ek"
        }
    ],
    }
    chatCtrl.save(newChat);
})

module.exports = router;