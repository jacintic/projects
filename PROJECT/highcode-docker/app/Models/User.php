<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    //Login user db fetch of user and pass to match against form data
    public function scopeLogin($query, $userAndPass)
    { 
        return $query->select('username', 'password')->where($userAndPass)->get();
    }

    // filter for HOME PAGE (cards)
    public function scopeTech($query, $tech)
    {
        return $query->join('tutorials', 'users.id', '=', 'tutorials.author')->where('tech', '=', $tech)->get();
    }

    // Filter for tutorial by id
    public function scopeId($query, $id)
    {
        return $query->join('tutorials', 'users.id', '=', 'tutorials.author')->where('tutorials.id', '=', $id)->get();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
