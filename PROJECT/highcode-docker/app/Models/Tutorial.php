<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Tutorial extends Model
{  
    use HasFactory; 
    
    // filter for HOME PAGE (cards)
    public function scopeTech($query,$tech) {
        return $query->join('users','tutorials.author','=','users.id')->where('tech','=',$tech)->get();
    }
    
    public function scopeLevels($query,$diff) {
        return $query->whereIn('diff',$diff);        
    }

    public function scopeTechf($query,$tech) {
        return $query->whereIn('tech',$tech);        
    }

    public function scopeDuration($query,$duration) {
        return $query->whereIn('duration',$duration);        
    }

   //filter for searchbar
    public function scopeSearch($query,$title,$tipo) {
        return $query->join('users','tutorials.author','=','users.id')->where($tipo,'like',"%$title%")->get();
    }

    /**
     * Insert tutorial on DB
     */
    public function addTutorial($r) {
        $tutorial = new Tutorial;
        $tutorial->tech = $r["tech"];
        $tutorial->author = $r["author"];
        $tutorial->title = $r["title"];
        $tutorial->diff = $r["diff"];
        $tutorial->duration = $r["duration"];
        $tutorial->desc = $r["desc"];
        $tutorial->body = $r["body"];
        $tutorial->save();
    }
}
