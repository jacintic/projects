<?php

namespace App\Exceptions;

use Exception;

class UserPhotoException extends Exception
{
    //
    public function customMessage() {
        return 'No has subido ninguna foto';
    }
}
