<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tutorial;
use App\Models\User;
use App\Http\Requests\CreateTutorialRequest;
class TutorialController extends Controller
{
    public $tutorialModel;

    public function __construct()
    {
        $this->tutorialModel= new Tutorial();
        $this->userModel= new User();

    }

    //Show a tutorial
    public function seeTutorial($id){
        $query = User::id($id);
        $tutorial = $query[0];
        return view('bodyTutorial', compact('tutorial'));
    }

    //Filter for home page
    public function showTutorials($tech) {
        if ($tech == 'all') {       
            return view('listTutorials')->with
            (['tutoriales'=>$this->userModel->join('tutorials','users.id','=','tutorials.author')->get()]);
        }
        return view('listTutorials')->with(['tutoriales'=>$this->userModel->tech($tech)]);
    }
  
    //Filter for checkbox 
    public function filterForm(Request $request) { 
        $orderby = $request->get('orderBy');   
        $diff = $request->get('diff');
        $tech = $request->get('tech');
        $duration = $request->get('duration');
        $filtros = Tutorial::query()->join('users', 'tutorials.author', '=', 'users.id');

        if (!empty($diff)) {
            $filtros->levels($diff);
        }
        if (!empty($tech)) {
            $filtros->techf($tech);
        }
        if (!empty($duration)) {
            $filtros->duration($duration);
        }
        if (!empty($orderby)) {
            $filtros->orderBy('tech',  $orderby);
        }
        $tutoriales = $filtros->get();
        return view('axiosViews/listaTutoriales', compact('tutoriales')); 
    }

     // Filter searchbar
     public function filterSearch(Request $request) {
        $title = $request->get('buscar');
        $tipo = $request->get('tipo');
        $tutoriales = Tutorial::search($title,$tipo);
        return view('listTutorials',compact('tutoriales'));
    }

    //Insert tutorial
    public function addTutorials(CreateTutorialRequest $dataTutorials) {           
        $datos = $dataTutorials->all();
        $this->tutorialModel->addTutorial($datos);
        return view('confirmation/insertedTutorial');
     }

     //Follow tutorial
     public function followTutorial(Request $request) {
        $info = [$request->get('title'),$request->get('tech')];
        $followeds = $request->session()->get('seguidos', []);
        array_push($followeds, $info);
        $request->session()->put('seguidos', $followeds); 
        return redirect(url()->previous());    
     }

     public function removeFollows(Request $request) {
        $request->session()->put('seguidos',[]);        
        return redirect(url()->previous());
     }
}

