<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\UploadPhotographyService;

class UploadPhotograpyProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    public function register()
    {
        $this->app->bind('App\Services\UploadPhotographyService', function ($app) {
            return new UploadPhotographyService();
        });
    }
}