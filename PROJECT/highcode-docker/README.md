## Project HighCode

Comandos necesarios:
- Para iniciar/cargar el proyecto con docker -> `docker-compose up -d`
- Posteriormente nos aseguramos que haya cargado la BBDD(Este comando lo usamos en php:7.4-fpm, click derecho, attach shell) -> `php artisan migrate:fresh --seed`
- Host puerto 90, BBDD puerto 99(confirmar en archivo .env).
- Si sale el "error" -> "502 Bad Gateway nginx/1.19.7" hay que esperar a que cargue el docker.

