@extends('layouts.app')
@section('title', 'Sign Up')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/styleforms.css') }}">
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
@endsection
@section('content')
<div class="container">
  <div class="row">
    <div class="col-lg-10 col-xl-9 mx-auto">
      <div class="card card-signin flex-row my-5">
        <div class="card-img-left d-none d-md-flex">
          <!-- Background image for card set in CSS! -->
        </div>
        <div class="card-body">
          <h5 class="card-title text-center">Register</h5>
          <!-- <form class="form-signin" method="post" action="/signupUser"> -->
          <form class="form-signin" method="post" action={{route('users.addUser')}} enctype="multipart/form-data">
            @csrf
            <div class="form-label-group">
              <input type="text" id="inputUserame" name="username" class="form-control @error('username') is-invalid @enderror" placeholder="Username" required autofocus>
              <label for="inputUserame">Username</label>
            </div>
            @error('username')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-label-group">
              <input type="email" id="inputEmail" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" required>
              <label for="inputEmail">Email address</label>
            </div>
            @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <hr>

            <div class="form-label-group">
              <input type="password" id="inputPassword" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
              <label for="inputPassword">Password</label>
            </div>
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror

            <div class="form-label-group">
              <input type="password" id="inputConfirmPassword" class="form-control" placeholder="Password" required>
              <label for="inputConfirmPassword">Confirm password</label>
            </div>
            <div class="form-group">
              <label for="imagen">Imagen de Perfil</label>
              <input type="file" name="imagen" />
            </div>


            <hr>
            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
            <a class="d-block text-center mt-2 small" href="/login">Log in</a>
            {{--@if ($errors->any())--}}
            {{--<br>--}}
            {{--<div class="alert alert-danger">--}}
            {{--<ul>--}}
            {{--@foreach ($errors->all() as $error)--}}
            {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
            {{--</ul>--}}
            {{--</div>--}}
            {{--@endif--}}
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@if (session('error'))
<div class="alert alert-danger">{{ session('error') }}</div>
@endif
@if(app('request')->has('success'))
    @if(app('request')->input('success'))
    <div class="alert alert-success">
        Producto guardado con exito
    </div>
    @endif
@endif
@endsection