@extends('layouts.app')

<!-- title-->
@section('title', 'Become a Teacher!')

<!-- cards home css-->
@section('styles')
    <!--<link rel="stylesheet" type="text/css" href="css/createTutorial.css">-->
@endsection

<!-- cards home html-->
@section('content')    
    <div class="container border border-secondary">
        <div class="row justify-content-center p-3 m-3">            
            <form method="post" action="" class="p-3">            
                <h2>Sigues professor</h2>
                <br>
                @csrf
                <div class="form-row">
                    <div class="col-10">
                        <label for="titol">Curriculum vitae</label>
                        <input type="file" name="curriculum" class="form-control-file">
                    </div>
                </div>
                    <small>(.md or .pdf)</small>
                <div class="form-row mt-3">
                    <div class="col-10">
                        <label for="titol">Tecnologies que coneixes</label>
                        <input type="text" name="tech" class="form-control">
                    </div>
                </div>
                <hr>
                <span><b>Enllaços als teus projectes</b></span>
                <div class="form-row mt-3">       
                    <br>
                    <div class="col-5">
                        <label for="tittle-project">Titol</label>
                        <input type="text" name="tittle-project" class="form-control">
                    </div>
                    <div class="col-5">
                        <label for="tittle-project">Enllaç</label>
                        <input type="text" name="lin-project" class="form-control">
                    </div>
                    <div class="col-10">

                        <label for="titol">Breu descripció</label><br>  
                        <textarea name="descProject" id="" cols="60" rows="3" class="form-control"></textarea><br>
                    </div>
                </div>
                <button>Afegeix un altre enllaç</button>
                <button class="ml-3">Esborra l'anterior enllaç</button>
                <br>
                <hr>
                <div class="form-row mt-3 mb-3">                
                    <div class="col-10">
                        <label for="titol">Explica'ns sobre tu</label><br> 
                        <textarea name="description" id="" cols="50" rows="10" class="form-control"></textarea><br>
                    </div>
                </div>
                <input type="submit" value="Sol·licita" class="btn btn-primary">
            </form>
        </div>
    </div>
@endsection
