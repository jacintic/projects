
@extends('layouts.app')

<!-- title-->
@section('title', 'Settings')

<!-- cards home css-->
@section('styles')
    <!--<link rel="stylesheet" type="text/css" href="css/createTutorial.css">-->
@endsection

<!-- cards home html-->
@section('content')    
    <!-- User current-->
    <div class="container p-15">
        <div class="row">
            <div class="col-3">
                <img src="{{ asset('/img/UserImg.png') }}" class="img-fluid" alt="Imagen">
            </div>
            <div class="col-6 ">
                <h1>User name</h1>
                <h3>user@email.com</h3>
            </div>
        </div>
    </div>
    <!-- div for align form an button-->
    <div class="container">
        <div class="row">
            <!-- settings form  -->
            <div class="col-9">
                <div class="container border border-secondary">
                    <div class="row justify-content-center p-3 m-3">            
                        <form method="post" action="" class="p-3">            
                            <h3>Edita</h3>
                            <br>
                            @csrf
                            <div class="form-row">
                                <div class="col-5">
                                    <label for="titol">Nom d'usuari</label>
                                    <input type="text" name="username" class="form-control">
                                </div>
                                <div class="col-5">
                                    <label for="titol">Fotografia</label>
                                    <input type="file" name="userImage" class="form-control-file">
                                </div>
                            </div>
                            <div class="form-row mt-3">
                            </div>
                            <div class="form-row mt-3">       
                                <div class="col-5">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col">
                                                <label for="tittle-project">Correu electrònic</label>
                                                <input type="text" name="userEmail" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label for="tittle-project">Contrassenya</label>
                                                <input type="text" name="lin-project" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label for="tittle-project">Confirma la contrassenya</label>
                                                <input type="text" name="lin-project" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <label for="titol">Breu descripció</label><br>  
                                    <textarea name="descProject" id="" cols="60" rows="10" class="form-control"></textarea><br>
                                </div>
                            </div>
                            <input type="submit" value="Guarda els canvis" class="btn btn-primary rounded">
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <a href="/becomeTeacher">
                <button class="btn btn-outline-success btn-large">Sigues Professor</button>
                </a>
            </div>
        </div>

    </div>
@endsection
