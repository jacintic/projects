<!-- Layout with only navbar, styles and js-->
@extends('layouts.app')

<!-- title-->
@section('title', 'Welcome to HighCode!')

    <!-- cards home css-->
@section('styles')
    <link rel="stylesheet" type="text/css" href="css/cards.css">
@endsection

<!-- cards home html-->
@section('content')
    {{-- <div class="row justify-content-center py-5" style="background: url(img/bg-home.jpg);background-size: contain;">
        <div class="col" style="display:flex;justify-content:center;">
            <iframe width="1080" height="720"
                src="//www.youtube.com/embed/qHn0SJHL6Sk?rel=0&modestbranding=1&autohide=1&showinfo=0&controls=0"
                frameborder="0" allowfullscreen></iframe>
        </div>
    </div> --}}
    <div class="row justify-content-center py-5" style="background-color: #ffc700">
        <iframe width="1280" height="720" src="https://www.youtube.com/embed/2vaQ4WWer3Y" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
    </div>

    <div class="cardContainer">
        <div class="cards">

            <div class="card c1">
                <a href="/Javascript" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Javascript" class="cathegory">
                    <h2>JavaScript</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>

            <div class="card c2">
                <a href="/Java" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Java" class="cathegory">
                    <h2>Java</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>


            <div class="card c3">
                <a href="/Laravel" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Laravel" class="cathegory">
                    <h2>Laravel</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>


            <div class="card c4">
                <a href="/Vue" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Vue" class="cathegory">
                    <h2>Vue</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>


            <div class="card c5">
                <a href="/Bash-script" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Bash-script" class="cathegory">
                    <h2>Bash Script</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>


            <div class="card c6">
                <a href="/Bootstrap" class="takewhole">
                    <span class="spanWhole"></span>
                </a>
                <a href="/Bootstrap" class="cathegory">
                    <h2>Bootstrap</h2>
                </a>
                <div class="difficulty">
                    <a href="#beginner" class="beg">easy</a>
                    <a href="#intermediate" class="int">medium</a>
                    <a href="#high" class="high">high</a>
                    <a href="#all" class="all">all</a>
                </div>
            </div>
        </div>
    </div>
@endsection
