@extends('layouts.app')

<!-- title-->
@section('title', 'Tutorial!')

<!-- cards home html-->
@section('content')
    {{-- Replantearse formato del tutorial mostrado --}}
    <div class="container py-4">
        {{-- <div class="row justify-content-center">
            <h1 class="display-3">{{ $tutorial->title }}</h1>
        </div>
        <div class="row">
            <div class="col-10">
                <p class="lead"> {{ $tutorial->desc }} </p>

            </div>
            <div class="col-1 align-self-end">
                <div class="row">
                    <button type="button" class="btn btn-primary">Primary</button>
                </div>
            </div>
        </div> --}}


        <div class="">
            <h1 class="display-4"> {{ $tutorial->title }} </h1>
            <div class="row">
                <div class="col-11">
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet quam quidem nulla
                        quisquam illum quis atque. Quidem corrupti ea impedit suscipit laborum nisi quasi, porro debitis
                        exercitationem odio eius expedita.</p>
                </div>
                <div style="width:50px;">
                    <div class="col-1 align-self-end">
                        <div class="row">
                            <button type="button" class="btn btn-primary">Follow</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div style="width:50px;">
                    <img src="{{ asset('/img/UserImg.png') }}" alt="" class="img-fluid">
                </div>
                <div class="col-auto">
                    <br>
                    <span>{{ $tutorial->username}}</span>
                </div>
                <div class="col-auto">
                    <br>
                    <p class="font-italic">{{ date('d-m-yy', strtotime($tutorial->created_at)) }}<p>
                </div>
            </div>
            <hr>
            <div class="row">
                <p class="font-weight-bold">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Itaque atque, quaerat
                    harum sunt
                    voluptatibus similique error quis reiciendis eveniet incidunt sed recusandae excepturi hic fuga, omnis
                    ipsam in distinctio natus.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet suscipit impedit quasi delectus,
                    repudiandae mollitia eveniet facere! At quod labore vitae dicta soluta aliquam ratione officia,
                    provident mollitia sit beatae?
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Asperiores vitae rem deserunt libero officia
                    nihil, velit similique officiis odit quos, expedita doloremque cupiditate eveniet autem minus illo
                    repudiandae. Consectetur, beatae?
                </p>

                <pre class="bg-secondary text-dark p-3"><code>&lt;p&gt;Sample text here...&lt;/p&gt;
                                &lt;p&gt;And another line of sample text here...&lt;/p&gt;
                                </code></pre>

                <p class="font-weight-bold">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Itaque atque, quaerat
                    harum sunt
                    voluptatibus similique error quis reiciendis eveniet incidunt sed recusandae excepturi hic fuga, omnis
                    ipsam in distinctio natus.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet suscipit impedit quasi delectus,
                    repudiandae mollitia eveniet facere! At quod labore vitae dicta soluta aliquam ratione officia,
                    provident mollitia sit beatae?
                </p>
                <p class="font-weight-bold">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Itaque atque, quaerat
                    harum sunt
                    voluptatibus similique error quis reiciendis eveniet incidunt sed recusandae excepturi hic fuga, omnis
                    ipsam in distinctio natus.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet suscipit impedit quasi delectus,
                    repudiandae mollitia eveniet facere! At quod labore vitae dicta soluta aliquam ratione officia,
                    provident mollitia sit beatae?
                </p>
            </div>
            <hr style="height:2px;border-width:0;color:gray;background-color:orange">
            <div class="container-fluid">
                <p class="h4">Comments:</p>
                <div class="container-fluid my-3">
                    <div class="row">
                        <div style="width:30px;">
                            <img src="{{ asset('/img/UserImg.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="col-auto">
                            <span>Anonimo</span>
                        </div>
                    </div>
                    <div class="row">
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. A saepe architecto voluptates
                            provident. Cumque iste, molestias explicabo earum assumenda, sapiente cum deserunt molestiae
                            repellat consequuntur dolore itaque alias dolores necessitatibus.</p>
                    </div>
                </div>
                <div class="container-fluid my-3">
                    <div class="row">
                        <div style="width:30px;">
                            <img src="{{ asset('/img/UserImg.png') }}" alt="" class="img-fluid">
                        </div>
                        <div class="col-auto">
                            <span>Paquirringo</span>
                        </div>
                    </div>
                    <div class="row">
                        <p>A saepe architecto voluptates provident. Cumque iste, molestias explicabo earum assumenda,
                            sapiente cum deserunt molestiae repellat consequuntur dolore itaque alias dolores
                            necessitatibus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
