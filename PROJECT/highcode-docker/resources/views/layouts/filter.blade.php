<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    <!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="CodedThemes">
    <meta name="keywords"
        content=" Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="CodedThemes">
    <!-- Favicon icon -->
    <!--<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon"> -->
    <!-- Google font-->
    <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet"> -->
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap/css/bootstrap.min.css') }}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/icon/themify-icons/themify-icons.css') }}">
    <!-- ico font -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/icon/icofont/css/icofont.css"> -->
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}">
    <!--<link rel="stylesheet" type="text/css" href="assets/css/jquery.mCustomScrollbar.css"> -->
    @yield('styles')
</head>

<body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="ti-menu"></i>
                        </a>
                        <a href="/">
                            <img class="img-fluid" src="{{ asset('/img/high-code.png') }}" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options">
                            <i class="ti-more"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li>
                                <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a>
                                </div>
                            </li>

                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="ti-fullscreen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="user-profile header-notification">
                                <a href="/signup">Sign Up</a>
                                <a href="/login">Log In</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="">
                                <div class="main-menu-content">
                                    <ul>
                                        <li class="more-details">
                                            <a href="#"><i class="ti-user"></i>View Profile</a>
                                            <a href="#!"><i class="ti-settings"></i>Settings</a>
                                            <a href="auth-normal-sign-in.html"><i
                                                    class="ti-layout-sidebar-left"></i>Logout</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!--<div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Sort Tutorials</div>-->
                            <!-- start  FORM-->
                            <!-- SEARCH FORM -->

                            <form method="GET" action="/filter">
                                <div class="input-group mb-3">
                                    <select name="orderby" class="custom-select ml-4 mt-4" id="inputGroupSelect01">
                                        <option value="">Order by...</option>
                                        <option value="desc">tech desc</option>
                                        <option value="asc">tech asc</option>
                                    </select>
                                </div>
                                <!-- LEVELS -->
                                <ul class="pcoded-item pcoded-left-item">
                                    <li class="pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                            <span class="pcoded-mtext"
                                                data-i18n="nav.basic-components.main">Levels</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="diff[]" value="easy">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Easy</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="diff[]" value="medium">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Medium</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="diff[]" value="high">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">High</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- LANGUAGES -->
                                <ul class="pcoded-item pcoded-left-item">
                                    <li class="pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                            <span class="pcoded-mtext"
                                                data-i18n="nav.basic-components.main">Languages</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Javascript">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">JavaScript</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Java">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Java</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Laravel">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Laravel</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Bash">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">BASH</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Vue">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">VUE</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="tech[]" value="Bootstrap">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">BOOTSTRAP</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <!-- DURATION -->
                                <ul class="pcoded-item pcoded-left-item">
                                    <li class="pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="ti-layout-grid2-alt"></i></span>
                                            <span class="pcoded-mtext"
                                                data-i18n="nav.basic-components.main">Duration</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="duration[]" value="short">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Short</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="duration[]" value="medium">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Medium</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="#">
                                                    <!--<span class="pcoded-micon"><i class="ti-angle-right"></i></span>-->
                                                    <input type="checkbox" class="form-check-input ml-1"
                                                        id="materialUnchecked" name="duration[]" value="long">
                                                    <label class="form-check-label ml-2"
                                                        for="materialUnchecked">Long</label>
                                                    <!--<span class="pcoded-mcaret"></span>-->
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <input class="mt-2 ml-5 rounded" type="button" value="Eviar!" onclick="formChecked()">

                            </form>
                            <!-- END FORM -->
                        </div>
                </div>
                </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    {{-- <div class="fixed-button"> --}}
    {{-- <a href="https://codedthemes.com/item/guru-able-admin-template/" target="_blank" class="btn btn-md btn-primary"> --}}
    {{-- <i class="fa fa-shopping-cart" aria-hidden="true"></i> Upgrade To Pro --}}
    {{-- </a> --}}
    {{-- </div> --}}
    </div>
    </div>
    <script src={{ asset('js/app.js') }}></script>
    <script>
        let formChecked = () => {
            //Recogemos elementos seleccionados del formulario
            let order = document.getElementById("inputGroupSelect01").value;
            let levels = [];
            let languages = [];
            let durat = [];
            //
            let levelValues = document.getElementsByName("diff[]");
            let langValues = document.getElementsByName("tech[]");
            let durationValues = document.getElementsByName("duration[]");

            for (let i = 0; i < levelValues.length; i++) {
                if (levelValues[i].checked == true) {
                    levels.push(levelValues[i].value);
                }
            }

            for (let i = 0; i < langValues.length; i++) {
                if (langValues[i].checked == true) {
                    languages.push(langValues[i].value);
                }
            }

            for (let i = 0; i < durationValues.length; i++) {
                if (durationValues[i].checked == true) {
                    durat.push(durationValues[i].value);
                }
            }

            axios({
                    method: 'get',
                    url: '/filter',
                    responseType: 'html',
                    params: {
                        orderBy: order,
                        diff: levels,
                        tech: languages,
                        duration: durat
                    },
                })
                .then(function(response) {
                    let datos = response.data;
                    let listaTutoriales = document.getElementById("mycontent");
                    listaTutoriales.innerHTML = datos;
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

    </script>
    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 9]>

<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery-ui/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/popper.js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="{{ asset('assets/js/modernizr/modernizr.js') }}"></script>
    <!-- am chart -->
    <script src="{{ asset('assets/pages/widget/amchart/amcharts.min.js') }}"></script>
    <script src="{{ asset('assets/pages/widget/amchart/serial.min.js') }}"></script>
    <!-- Todo js -->
    <script type="text/javascript " src="{{ asset('assets/pages/todo/todo.js') }}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{ asset('assets/pages/dashboard/custom-dashboard.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
    <script type="text/javascript " src="{{ asset('assets/js/SmoothScroll.js') }}"></script>
    <script src="{{ asset('assets/js/pcoded.min.js') }}"></script>
    <script src="{{ asset('assets/js/demo-12.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script>
        var $window = $(window);
        var nav = $('.fixed-button');
        $window.scroll(function() {
            if ($window.scrollTop() >= 200) {
                nav.addClass('active');
            } else {
                nav.removeClass('active');
            }
        });

    </script>
</body>

</html>
